<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-xs-12">
				<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			</div>
			<div class="col-lg-6 col-xs-12 navigation-menu">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
	         	</button>
				<?php get_template_part( 'template-parts/header/top', 'menu' ); ?>
			</div>
		</div>
	</div>
</div>