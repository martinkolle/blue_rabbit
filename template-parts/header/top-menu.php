<?php wp_nav_menu( array(
	'theme_location'	=> 'main',
	'menu_id' 			=> 'main-menu',
	'depth' 			=> 2,
	'container_class' 	=> 'collapse navbar-collapse',
	'container_id' 		=> 'navbar',
	'menu_class'		=> 'nav navbar-nav navbar-right',
	'fallback_cb' 		=> 'wp_bootstrap_navwalker::fallback',
	'walker' 			=> new wp_bootstrap_navwalker()
) );