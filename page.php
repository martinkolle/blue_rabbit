<?php 
/**
 * Page
 * 
 */
?>

<?php get_header(); ?>

<?php $tbs_text_width = ( is_active_sidebar( 'left' ) ) ? 'col-md-8 col-xs-12' : 'col-lg-12'; ?>

<section class="default-text-section row">
	<div class="container">
		<div class="row">
			<?php get_sidebar(); ?>

			<div class="<?php echo $tbs_text_width; ?> text-section">

			<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/page/content', 'page' );

				endwhile; // End of the loop.
				?>

			</div>
		</div>
	</div>
</section>

<?php get_footer();