<?php
/**
 * Search archive view
 * 
 */

get_header(); ?>

<section class="post-archive-section">

	<div class="row">

		<div class="container">

			<div class="row">
				<div class="col-xs-12">
					<header class="page-header">
						<?php if ( have_posts() ) : ?>
							<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'blue_rabbit' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
						<?php else : ?>
							<h1 class="page-title"><?php _e( 'Nothing Found', 'blue_rabbit' ); ?></h1>
						<?php endif; ?>
					</header>
				</div>

				<?php
				if ( have_posts() ) : ?>
				<div class="col-xs-12">
					<?php 
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/post/content', 'excerpt' );

					endwhile;

					the_posts_pagination();

				endif; ?>
				</div>

			</div>

		</div>

	</div>

</section>

<?php get_footer();