<?php
/**
 * Template Name: Contact
 *
 * Simple contact page with map (ACF fields) and sidebar
 */
?>

<?php get_header(); ?>
<?php $tbs_text_width = ( is_active_sidebar( 'contact' ) ) ? 'col-md-8 col-xs-12' : 'col-xs-12'; ?>

<section class="default-page-section default-text-section row">

	<div class="container">
		<div class="row">
			<?php get_sidebar( 'contact' ); ?>

			<div class="<?php echo $tbs_text_width; ?> text-section">

			<?php if( $location = get_field('address') ) : ?>
				<div class="map-wrapper">
					<div id="map" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
			<?php endif; ?>

			<?php
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/page/content', 'page' );
				endwhile; // End of the loop.
				?>

			</div>
		</div>

	</div>

</section>

<?php get_footer();