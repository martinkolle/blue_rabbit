<?php
/**
 * Static frontpage
 * 
 */
?>

<?php get_header(); ?>

<?php if( have_rows('frontpage_slides') ): ?>
<section class="hero-slider-section row">
	<ul class="hero-slider">

	<?php while( have_rows('frontpage_slides') ): the_row(); 
		// vars
		$image = get_sub_field('image');
		?>

		<li class="slide">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
		</li>

	<?php endwhile; ?>

	</ul>
</section>
<?php endif; ?>

<section class="quick-links-section">

    <div class="container">
        <div class="col-lg-12">
            <h2><?php _e( 'Popular products', 'blue_rabbit'); ?></h2>
        </div>
    </div>

    <div class="container">
    	<?php echo do_shortcode( '[best_selling_products  per_page="4" columns="4"]' ); ?>
    </div>

</section>

<?php get_footer();