<?php 
/**
 * Sidebar contact
 * 
 */
?>
<?php if( is_active_sidebar( 'contact' ) ) : ?>

	<aside class="col-lg-4 col-xs-12 sidebar-left sidebar-contact">
		<?php dynamic_sidebar( 'contact' ); ?>
	</aside>

<?php endif;