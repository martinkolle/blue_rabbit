var init_map = function(){

    var map_canvas = $('#map');

    //if no map div found - just return
    if( map_canvas.length == 0 )
        return;

    var map_lat 	= parseFloat(map_canvas.attr('data-lat')),
        map_lng 	= parseFloat(map_canvas.attr('data-lng')),
        map_address = {lat: map_lat, lng: map_lng};

    //Init the map - one map pr. page
    var map = new google.maps.Map(map_canvas[0], {
        center: map_address,
        zoom: 18
    });

    //Add a marker to the map
    var marker = new google.maps.Marker({
        position: map_address,
        map: map
    });
};