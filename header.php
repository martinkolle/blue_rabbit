<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="main-wrap container-fluid">
    <div class="top-bar row">
        <?php //This includes search bar ?>
        <?php get_template_part( 'template-parts/header/top', 'bar' ); ?>
    </div>
    <header class="top">
        <?php //This includes site-branding (logo) and top menu ?>
        <?php get_template_part( 'template-parts/header/header', 'top' ); ?>       
    </header>