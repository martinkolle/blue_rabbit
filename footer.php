<?php if ( is_active_sidebar( 'footer' ) ) : ?>
<footer class="row footer">
    <div class="container widget-area">
    	<div class="row">
			<?php dynamic_sidebar( 'footer' ); ?>
		</div>
	</div>
</footer>
<?php endif; ?>

<div class="row copyright">
    <div class="container">
        <div class="col-lg-12">
            © <?php echo date( 'Y' ); ?> HTML24, All rights reserved
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</div>
</body>
</html>