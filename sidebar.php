<?php 
/**
 * Sidebar left
 * 
 */
?>

<?php if( is_active_sidebar( 'left' ) ) : ?>

	<aside class="col-lg-4 col-xs-12 sidebar-left">
		<?php dynamic_sidebar( 'left' ); ?>
	</aside>

<?php endif;