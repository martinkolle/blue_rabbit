<?php
/**
 * Blue Rabbit theme
 *
 * @author Martin Kollerup <martin.kollerup@gmail.com>
 * @version 1.0.0
 */

/**
 * Set up theme deafult and register wp features
 * Hooked into 'after_setup_theme'
 *
 * Posible to override by a child-theme
 */

if( !function_exists( 'br_theme_setup' ) ) :
	
function br_theme_setup() {

	//No hard-coded <title> in header.php
	add_theme_support( 'title-tag' );

	//Change theme logo
	add_theme_support( 'custom-logo', array(
		'height'      => 110,
		'width'       => 200,
		'flex-height' => true
	) );

	//Thumbnails in posts
	add_theme_support( 'post-thumbnails' );

	//Enable wp features to return in html5 markup
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	//Woocommerce support
	add_theme_support( 'woocommerce' );
	remove_action('wp_head','wc_generator_tag');

	// Set the default content width.
	$GLOBALS['content_width'] = 1170;

	// Register main menu
	register_nav_menus( array(
		'main' => __( 'Main Menu', 'blue_rabbit' ),
	) );

	//Include bootstrap nav walker
	require_once('inc/wp_bootstrap_navwalker.php');

	//remove wordpress version number
	remove_action('wp_head', 'wp_generator');
}

endif; //br_theme_setup

add_action( 'after_setup_theme', 'br_theme_setup' );


/**
 * Add widgets
 *
 * Override in child theme: unregister with a priority 11 or higher 
 * on widgets_init
 */

function br_widgets_init() {

	register_sidebar( array(
		'name'          => __('Footer', 'blue_rabbit'),
		'description' 	=> __('Add widgets to the footer. The ideal is 3 widgets.', 'blue_rabbit'),
		'id'            => 'footer',
		'before_widget' => '<div id="%1$s" class="%2$s col-lg-4 col-xs-12">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __('Left', 'blue_rabbit'),
		'description' 	=> __('Add widgets to left sidebar.', 'blue_rabbit'),
		'id'            => 'left',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __('Contact', 'blue_rabbit'),
		'description' 	=> __('Add widgets to left sidebar on contact page.', 'blue_rabbit'),
		'id'            => 'contact',
		'before_widget' => '<div id="%1$s" class="%2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'br_widgets_init' );


/**
 * Enqueue scripts and styles to wp_head or wp_footer
 * 
 */

function br_enqueue_scripts() {

	wp_enqueue_style( 'br-frontend', get_template_directory_uri() . '/css/main.css' );

	//Include bootstrap
	wp_enqueue_script( 'br-bootstrap', get_template_directory_uri() . '/includes/bootstrap.min.js', array( 'jquery' ), '3.3.7', true );

	//Include bxslider - frontpage slider
	wp_enqueue_script( 'br-bxslider', get_template_directory_uri() . '/includes/jquery.bxslider.min.js', array( 'jquery' ), '4.1.2', true );

	if( is_page_template( 'page-templates/contact.php' ) ) {
		//Google maps api - remember API key!
		//async attr will be added in "br_script_async_attr"
		wp_enqueue_script( 'google-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB5rL9EncVBEV5s3rmVvB6zTRmgo5bgNSw', false, false, true );
	}

	//Include main script - inits and modifications run from here
	wp_enqueue_script( 'br-functions', get_template_directory_uri() . '/js/main.js', array( 'jquery' ), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'br_enqueue_scripts' );


/**
 * Google Maps Api Key for ACF
 * 
 */

if( !function_exists( 'br_acf_init' ) ) :

function br_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyB5rL9EncVBEV5s3rmVvB6zTRmgo5bgNSw');
}

endif;

add_action('acf/init', 'br_acf_init');


/**
 * Add async attr to script elements
 *
 * Posible to override by child
 */

if( !function_exists( 'br_script_async_attr' ) ) :

function br_script_async_attr( $tag, $handle ) {
   
   //add script handles below
   $scripts_to_async = array('google-api');
   
   foreach( $scripts_to_async as $async_script ) {
      if ($async_script === $handle) {
         return str_replace( ' src', ' async="async" src', $tag );
      }
   }
   return $tag;
}

endif;

add_filter('script_loader_tag', 'br_script_async_attr', 10, 2);


/**
 * Woocommerce related functions and actions
 * 
 */

/**
 * Remove ratings from loops
 * 
 */
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );


/**
 * Disable emoji's - not often used at business sites
 * 
 */

/**
 * Disable the emoji's
 * 
 * @author  Ryan Hellyer
 * @license GPL v.2
 */

if( !function_exists( 'disable_emojis' ) ) :

function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * @author  Ryan Hellyer
 * @license GPL v.2
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

endif;


/**
 * Remove 
 */