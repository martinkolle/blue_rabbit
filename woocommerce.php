<?php 
/**
 * Woocommerce wrapper
 * All woocommerce pages will be loaded through this wrapper
 * 
 */
?>

<?php get_header(); ?>
    <section class="breadcrumb-wrap row clearfix">
        <div class="container">
            <?php woocommerce_breadcrumb(); ?>
        </div>
    </section>

    <section class="products-section">
        <div class="container">
            <?php woocommerce_content(); ?>        
        </div>
    </section>
<?php get_footer();