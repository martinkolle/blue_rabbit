<?php
/**
 * Archive and category
 * 
 */

get_header(); ?>

	<section class="post-archive-section">

		<div class="row">

			<div class="container">

				<div class="row">
					<?php if ( have_posts() ) : ?>
					<div class="col-xs-12">
						<header class="page-header">
							<?php
								the_archive_title( '<h1 class="page-title">', '</h1>' );
								the_archive_description( '<div class="taxonomy-description">', '</div>' );
							?>
						</header>
					</div>
					<?php endif; ?>

					<?php
					if ( have_posts() ) : ?>
					<div class="col-xs-12">
						<?php 
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/post/content', get_post_format() );

						endwhile;

						the_posts_pagination();

						endif; ?>
					</div>

				</div>

			</div>

		</div>

	</section>

<?php get_footer();