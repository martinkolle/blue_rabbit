Blue Rabbit
============

At HTML24 we do a lot of projects in Wordpress and WooCommerce.
That's why we've made this little test for you.

* Fork this Wordpress/Woocommerce theme
* Setup a local installation of Wordpress with WooCommerce
* Setup gulp in your project
* Implement the theme in Wordpress and setup the Webshop
* Make a pull request to this project

Components
----------
When you have completed this test successfully, you will have a basic understanding of:

* git
* Wordpress theme implementation
* WooCommerce
* PHP
* gulp

The task
--------

Throughout this theme there are some static pages that need to be implemented the proper way.
These pages includes the front page, the product archive, the single product page etc.
When handing in the test include the database and a zip file of the uploads and plugins you've used.


Developer notes
--------
ACF is the only plugin I have used. SQL dump and ACF (PRO) is in "sql_extensions".


What's done?
--------
1. **Implement blue_rabbit theme** as installable Wordpress theme.
2. Sidebar and footer is using widgets (different positions)
3. Slider on frontpage - using ACF repeater field for dynamic slides
4. Contact-page: Google Map position can be changed (depending on post type)
5. **Install woocommmerce** (using std. demo-data) and a simple setup
6. Run gulp
7. Added bootstrap-styl to gulpfile.js and package.json
8. Compress CSS on compile

What's not done?
--------
* Editor.css is not compiled as a gulp task
* Mobile optimizing (eg. mobile menu)
* Schema on address
* An exhaustive survey of the design (I call it pixelperfect-review)

What else could be done?
--------
* Use a slider plugin (eg. revolution slider)